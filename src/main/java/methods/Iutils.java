package methods;

import java.awt.AWTException;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public interface Iutils {

	public void openBrowser(WebDriver driver,String baseUrl);
	public void exitBrowser(WebDriver driver);
	public WebElement locateElement(WebDriver driver,String xpath);
	public List<WebElement> locateMultipleElements(WebDriver driver,String xpath);
	public void clickElement(WebElement element);
	public boolean verifyElement(WebDriver driver,String xpath);
	public boolean EqualityCheck(int a, int b,String function);
	public WebElement wait(WebDriver driver,String xpath);
	public void enterText(WebElement element,String text);
	public void sleep(String time) throws InterruptedException;
	public int getSize(List<WebElement> elements);
	public void stringEqual(String s1,String s2);
	public void performActionHover(WebDriver driver,WebElement element);
	public void performActionDragAndDrop(WebDriver driver,WebElement element,int x,int y);
	public void frameSwitching(WebDriver driver,WebElement element);
	public String getMessage(WebElement element);
	public String switchWindow(WebDriver driver);
	public void switchToMainWindow(WebDriver driver, String mainWindow);
	public boolean isDisplayed(WebElement element);
	public void scrollWindow(int a, int b, WebDriver driver);
	public void scrollUsingRobot(int wheels) throws AWTException;
}
