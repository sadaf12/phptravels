package methods;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Utils implements Iutils {
	
	public void openBrowser(WebDriver driver, String baseUrl) {

		driver.get(baseUrl);
		driver.manage().window().maximize();
	}

	public void exitBrowser(WebDriver driver) {
		driver.quit();
	}

	public WebElement locateElement(WebDriver driver, String xpath) {
		WebElement element = driver.findElement(By.xpath(xpath));
		return element;
	}

	public List<WebElement> locateMultipleElements(WebDriver driver, String xpath) {
		List<WebElement> element = driver.findElements(By.xpath(xpath));
		return element;
	}

	public void clickElement(WebElement element) {
		element.click();

	}

	public boolean verifyElement(WebDriver driver, String xpath) {
		int size = driver.findElements(By.xpath(xpath)).size();

		if (size > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean EqualityCheck(int a, int b, String function) {

		int ch = 0;
		if (function == "lessOrEqual") {
			ch = 1;
		} else if (function == "equalOrNot") {
			ch = 2;
		} else if (function == "greaterOrNot") {
			ch = 3;
		}

		switch (ch) {

		case 1:
			if (a <= b) {
				return true;
			} else {
				return false;
			}
		case 2:
			if (a == b) {
				return true;
			} else {
				return false;
			}
		case 3:
			if (a >= b) {
				return true;
			} else {
				return false;
			}
		default:
			return false;

		}

	}

	public WebElement wait(WebDriver driver, String xpath) {

		@SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));

		return element;
	}

	public void enterText(WebElement element, String text) {

		element.sendKeys(text);

	}

	public void sleep(String time) throws InterruptedException {

		if (time == "long") {
			Thread.sleep(8000);
		} else if (time == "short") {
			Thread.sleep(5000);
		} else if (time == "milli") {
			Thread.sleep(2000);
		}
	}

	public int getSize(List<WebElement> elements) {

		return elements.size();
	}

	public void stringEqual(String s1, String s2) {

		Assert.assertEquals(s1, s2);

	}

	public void performActionHover(WebDriver driver, WebElement element) {

		Actions action1 = new Actions(driver);

		action1.moveToElement(element).perform();

	}

	public void performActionDragAndDrop(WebDriver driver, WebElement element, int x, int y) {

		Actions action = new Actions(driver);
		action.dragAndDropBy(element, x, y).perform();
	}

	public void frameSwitching(WebDriver driver, WebElement element) {

		driver.switchTo().frame(element);
	}

	public String getMessage(WebElement element) {

		return element.getText();
	}

	public String switchWindow(WebDriver driver) {

		String mainWindow = driver.getWindowHandle();

		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();

		while (i1.hasNext()) {
			String ChildWindow = i1.next();
			if (!mainWindow.equalsIgnoreCase(ChildWindow)) {
				driver.switchTo().window(ChildWindow);
			}
		}

		return mainWindow;

	}

	public void switchToMainWindow(WebDriver driver, String mainWindow) {

		driver.close();
		driver.switchTo().window(mainWindow);

	}

	public boolean isDisplayed(WebElement element) {

		boolean bool = element.isDisplayed();

		return bool;

	}

	public void scrollWindow(int a, int b, WebDriver driver) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(" + a + "," + b + ")", "");
	}

	public void scrollUsingRobot(int wheels) throws AWTException {
		Robot robot = new Robot();
		robot.mouseWheel(wheels);
	}
}
