package Locators;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class GetProperty {

	public Properties property;
	
	public String getProperty(String filename, String attribute) throws IOException {
			
		FileInputStream input;
		property = new Properties();
		
		if(filename=="Affiliate") {
			input = new FileInputStream("D:\\SeleniumAutomatedProject\\Academy_testing\\src\\main\\java\\Locators\\Affiliate.properties");
			property.load(input);
			return property.getProperty(attribute);
		}
		else if(filename=="AffiliateRegistration") {
			input = new FileInputStream("D:\\SeleniumAutomatedProject\\Academy_testing\\src\\main\\java\\Locators\\AffiliateRegistration.properties");
			property.load(input);
			return property.getProperty(attribute);
		}
		else if(filename=="HomePage") {
			input = new FileInputStream("D:\\SeleniumAutomatedProject\\Academy_testing\\src\\main\\java\\Locators\\HomePage.properties");
			property.load(input);
			return property.getProperty(attribute);
		}
		else if(filename=="Pricing") {
			input = new FileInputStream("D:\\SeleniumAutomatedProject\\Academy_testing\\src\\main\\java\\Locators\\Pricing.properties");
			property.load(input);
			return property.getProperty(attribute);
		}
		else if(filename=="Technology") {
			input = new FileInputStream("D:\\SeleniumAutomatedProject\\Academy_testing\\src\\main\\java\\Locators\\Technology.properties");
			property.load(input);
			return property.getProperty(attribute);
		}
		else{
			return null;
		}
		
		
		
		
	}
	
	
}
