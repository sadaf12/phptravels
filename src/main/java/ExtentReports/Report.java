package ExtentReports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class Report {
	
	public ExtentHtmlReporter htmlReporter;
	public ExtentReports extent;
	public ExtentTest test;
	
	public void reportGeneration() {
		
	htmlReporter = new ExtentHtmlReporter("./test-output/Reports/HtmlReport.html");
	extent = new ExtentReports();
	extent.attachReporter(htmlReporter);

	extent.setSystemInfo("OS", "Windows");
	extent.setSystemInfo("HostName", "Selenium User");
	extent.setSystemInfo("Environment", "QA");

	htmlReporter.config().setDocumentTitle("Automation Report");
	htmlReporter.config().setReportName("Extent Report");
	htmlReporter.config().setTheme(Theme.DARK);
	
	}
		
}
