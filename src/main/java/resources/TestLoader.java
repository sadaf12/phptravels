package resources;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestLoader {

	public static WebDriver driver;
	public Properties prop;

	public WebDriver initializeDriver() throws IOException {
		
		prop = new Properties();
		FileInputStream fs = new FileInputStream(
				"D:\\SeleniumAutomatedProject\\Academy_testing\\src\\main\\java\\resources\\data.properties");
		prop.load(fs);
		String browserName = prop.getProperty("browser");

		if (browserName.equalsIgnoreCase("chrome")) {

			System.setProperty(prop.getProperty("chrome"),prop.getProperty("chromeDriverPath"));
			driver = new ChromeDriver();
		}else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty(prop.getProperty("firefox"), prop.getProperty("firefoxDriverPath"));

			driver = new FirefoxDriver();

		}else if (browserName.equalsIgnoreCase("IE")) { // execute in IE

		}

		driver.manage().window().maximize();
		return driver;

	}
	
}
