package Test_Cases;

import java.awt.AWTException;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Locators.GetProperty;
import methods.Utils;

public class Tests_2 extends BaseTest {
  
    Utils object=new Utils();
    
    GetProperty get = new GetProperty();
    
    List<WebElement> elements;
    WebElement element;
    WebElement element2;
    
    //Verify that on clicking the pricing, it should redirects to the plans and prices page.
    @Test(priority=0)
    public void verify_Pricing_Page() throws InterruptedException, IOException {
    	
    	report.test = report.extent.createTest("verify_Pricing_Page");
    	element = object.locateElement(driver, get.getProperty("HomePage", "pricing"));
    	object.clickElement(element);
    	object.sleep("short");
    	String title = driver.getTitle();
    	String expectedtitle = "Order purchase script onetime payment - PHPTRAVELS";    	
    	Assert.assertEquals(title, expectedtitle);
    	
    	}
    
    //Verify if there are four plans available.
    @Test(priority=1)
    public void verify_no_of_plans() throws IOException {
    	
    	report.test = report.extent.createTest("verify_no_of_plans");
    	elements = object.locateMultipleElements(driver, get.getProperty("Pricing", "pricesAndPlans"));
    	int size = elements.size();
    	int expectedSize = 4;
    	Assert.assertEquals(size, expectedSize);
    	
    }
    
    //Verify on clicking 'contact us ' button , it should redirect us to the Contact Us page.
    @Test(priority=2)
    public void verify_contact_page() throws InterruptedException, IOException, AWTException {
    	
    	report.test = report.extent.createTest("verify_contact_page");
    	object.scrollWindow(0, 2000, driver);
    	//object.scrollUsingRobot(20);
    	element = object.wait(driver, get.getProperty("Pricing", "contactUsButton"));
    	object.clickElement(element);
    	object.sleep("short");
    	String title = driver.getTitle();
    	String expected = "Contact Us - PHPTRAVELS";
    	Assert.assertEquals(title, expected);
    	
    }
}
