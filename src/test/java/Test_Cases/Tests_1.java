package Test_Cases;

import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Locators.GetProperty;
import methods.Utils;

public class Tests_1 extends BaseTest {

	Utils object = new Utils();

	GetProperty get = new GetProperty();
	
	List<WebElement> elements;
	WebElement element;
	WebElement element2;

	// Verify the title of homepage .
	@Test(priority = 0)
	public void verify_Title() {

		report.test = report.extent.createTest("verify_Title");
		String title = driver.getTitle();
		String expectedTitle = "Demo Script Test drive - PHPTRAVEL";
		Assert.assertEquals(title, expectedTitle);

	}

	// Verify if user hovers over the features, the dropdown should appear.
	@Test(priority = 1)
	public void verify_features_dropdown() throws InterruptedException, IOException {
		
		report.test = report.extent.createTest("verify_features_dropdown");
		element = object.locateElement(driver, get.getProperty("HomePage", "features"));
		object.performActionHover(driver, element);
		object.sleep("milli");
		element2 = object.locateElement(driver, get.getProperty("HomePage", "featuresDropdown"));
		Boolean appears = object.isDisplayed(element2);
		Assert.assertTrue(appears);

	}

	// Verify if user hovers over the product, the dropdown should appear.
	@Test(priority = 2)
	public void verify_product_dropdown() throws InterruptedException, IOException {

		report.test = report.extent.createTest("verify_product_dropdown");
		element = object.locateElement(driver, get.getProperty("HomePage", "product"));
		object.performActionHover(driver, element);
		object.sleep("milli");
		element2 = object.locateElement(driver, get.getProperty("HomePage", "productDropdown"));
		Boolean appears = object.isDisplayed(element2);
		Assert.assertTrue(appears);
		
	}

	// Verify if select 'Technology', it should redirects to the technology page.
	@Test(priority = 3)
	public void select_Technology() throws InterruptedException, IOException {

		report.test = report.extent.createTest("select_Technology");
		element = object.locateElement(driver, get.getProperty("HomePage", "productTechnology"));
		object.clickElement(element);
		object.sleep("short");
		element2 = object.locateElement(driver, get.getProperty("Technology", "technologyTiltle"));
		String title = object.getMessage(element2);
		String expectedTitle = "Technology";
		Assert.assertEquals(title, expectedTitle);

	}

	// Verify there are eight technolgies.
	@Test(priority = 4)
	public void verify_Technologies() throws IOException {
		
		report.test = report.extent.createTest("verify_Technologies");
		elements = object.locateMultipleElements(driver, get.getProperty("Technology", "noOfTechnologies"));
		int size = elements.size();
		int expectedSize = 8;
		Assert.assertEquals(size, expectedSize);
		
	}
}
