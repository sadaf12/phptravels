package Test_Cases;

import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Locators.GetProperty;
import methods.Utils;


public class Tests_3 extends BaseTest{

    Utils object=new Utils(); 
    GetProperty get = new GetProperty();
    List<WebElement> elements;
    WebElement element;
    WebElement element2;
    String mainwindow;
		
	//Verify if user selects affiliate under company, it should redirects user to the 'Affiliate page'.
	@Test(priority=0)
	public void verify_Affiliate_Page() throws InterruptedException, IOException {
		
		report.test = report.extent.createTest("verify_Affiliate_Page");
		element = object.locateElement(driver, get.getProperty("HomePage", "company"));
		object.performActionHover(driver, element);
		element2 = object.locateElement(driver, get.getProperty("HomePage", "companyAffiliate"));
		object.clickElement(element2);
		object.sleep("short");
		String title = driver.getTitle();
		String expectedTitle = "Become an Affiliate Partner - PHPTRAVELS";
		Assert.assertEquals(title, expectedTitle);
		
	}
	
	//Verify if user clicks on 'Join now' button, it should opens  a "Affiliate Registration" form.
	@Test(priority=1)
	public void verify_join_now() throws InterruptedException, IOException{
		
		report.test = report.extent.createTest("verify_join_now");
		element = object.locateElement(driver, get.getProperty("Affiliate", "joinNowButton"));
		object.clickElement(element);
		object.sleep("short");
		mainwindow = object.switchWindow(driver);
        element = object.locateElement(driver, get.getProperty("AffiliateRegistration", "formHeading"));
        String title = object.getMessage(element);
        String expectedTitle = "Affiliate Registration";
        Assert.assertEquals(title, expectedTitle);
              
        }
     //Verify that the user is able to successfully submit the 'Affiliate registration' form.
	@Test(priority=2)
	public void submit_form() throws InterruptedException, IOException {
		
		report.test = report.extent.createTest("submit_form");
		
		element = object.locateElement(driver, get.getProperty("AffiliateRegistration", "name"));
		object.enterText(element, "Sadaf");
		object.sleep("milli");
		
		element = object.locateElement(driver, get.getProperty("AffiliateRegistration", "email"));
		object.enterText(element, "abc@gmail.com");
		object.sleep("milli");
		
		element = object.locateElement(driver, get.getProperty("AffiliateRegistration", "phone"));
		object.enterText(element, "1122334455");
		object.sleep("milli");
		
		element = object.locateElement(driver, get.getProperty("AffiliateRegistration", "website"));
		object.enterText(element, "abc.com");
		object.sleep("milli");
		
		element = object.locateElement(driver, get.getProperty("AffiliateRegistration", "monthlyTrafficFirstCheck"));
		object.clickElement(element);
		object.sleep("milli");
		
		element = object.locateElement(driver, get.getProperty("AffiliateRegistration", "monthlyTrafficSecondCheck"));
		object.clickElement(element);
		object.sleep("milli");
		
		element = object.locateElement(driver, get.getProperty("AffiliateRegistration", "monthlyTrafficThirdCheck"));
		object.clickElement(element);
		object.sleep("milli");
		
		element = object.locateElement(driver, get.getProperty("AffiliateRegistration", "salesSecond"));
		object.clickElement(element);
		object.sleep("milli");
		
		element = object.locateElement(driver, get.getProperty("AffiliateRegistration", "submitButton"));
		object.clickElement(element);
		object.sleep("milli");
		
		element2 = object.locateElement(driver, get.getProperty("AffiliateRegistration", "formSubmitted"));
		String text = object.getMessage(element2);
		String expected = "Thank you for registration we will contact you back soon to setup your account. ";
		Assert.assertEquals(text, expected);
		
		object.switchToMainWindow(driver, mainwindow);
				
	}

}
