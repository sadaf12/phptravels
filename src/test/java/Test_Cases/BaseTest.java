package Test_Cases;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import ExtentReports.Report;
import methods.Utils;
import resources.TestLoader;

public class BaseTest {

	public WebDriver driver;
	
	TestLoader loader = new TestLoader();
	Utils object = new Utils();
	public Report report = new Report();
	
	@BeforeTest
	public void initialize() throws IOException {
		driver=loader.initializeDriver();
		driver.get(loader.prop.getProperty("url"));
		report.reportGeneration();
	}
	
	@AfterTest
	public void terminateBrowser() {
		object.exitBrowser(driver);
		report.extent.flush();
	}
	
	@AfterMethod
	public void getResult(ITestResult result) {
		if(result.getStatus()==ITestResult.FAILURE)
		{
			report.test.fail(result.getThrowable());
			
		}else if(result.getStatus()==ITestResult.SKIP)
		{
			
			report.test.skip(result.getThrowable());
		}else
		{
			report.test.pass(result.getName()+"Test Passed");
		}
	}
	
	
}
